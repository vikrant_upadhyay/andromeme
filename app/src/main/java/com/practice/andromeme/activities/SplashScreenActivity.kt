package com.practice.andromeme.activities

import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.practice.andromeme.MainActivity
import com.practice.andromeme.R
import kotlin.system.exitProcess

@Suppress("DEPRECATION")
class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        Handler().postDelayed({
            connectionCheck()
        }, 3000)
    }

    fun connectionCheck() {
        val connectivityManager = getSystemService(ConnectivityManager::class.java)
        val currentNetwork = connectivityManager.getActiveNetwork()
        if (currentNetwork != null) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            allertForRetry()
        }
    }

    private fun allertForRetry() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.alertDialogHeader))
        builder.setMessage(getString(R.string.messageAlertDialog))
        builder.setInverseBackgroundForced(false)
        builder.setPositiveButton(getString(R.string.retryforinternet)) { dialog, which ->
            connectionCheck()
        }

        builder.setNeutralButton(getString(R.string.exitdirectly)) { dialog, which ->
            moveTaskToBack(true);
            exitProcess(-1)
        }
        builder.show()
    }

}