package com.practice.andromeme.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.practice.andromeme.databinding.FragmentDrinkDetailBinding
import com.practice.andromeme.viewmodels.DrinkViewModel

class DrinkDetailFragment : Fragment() {
    //SHARED-VIEWMODEL ...
    lateinit var binding: FragmentDrinkDetailBinding
    val viewModel: DrinkViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDrinkDetailBinding.inflate(inflater)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        settingArg()
        return binding.root
    }

    private fun settingArg() {
        binding.textViewOnDetails.text = getArguments()?.getString("NAME")
        binding.textViewOnDetails.setOnClickListener {
            viewModel.getDataByNAme(binding.textViewOnDetails.text.toString())
        }
    }
}