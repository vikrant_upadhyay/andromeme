package com.practice.andromeme.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.practice.andromeme.adapters.RecyclerViewAdapter
import com.practice.andromeme.databinding.FragmentDashboardBinding
import com.practice.andromeme.viewmodels.DrinkViewModel

class DashboardFragment : Fragment() {
    //Shared View model
    private val viewModel: DrinkViewModel by activityViewModels()
    lateinit var binding: FragmentDashboardBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        binding = FragmentDashboardBinding.inflate(inflater)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        binding.recyclerOnDashboard.adapter = RecyclerViewAdapter()
        return binding.root
    }



}