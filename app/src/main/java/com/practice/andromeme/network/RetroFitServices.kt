package com.practice.andromeme.network

import com.practice.andromeme.MemeConstants
import com.practice.andromeme.models.Drink
import com.practice.andromeme.models.DrinksListObj
import kotlinx.coroutines.flow.Flow
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface RetroFitServices {

    @Headers("Content-Type: application/json")
    @GET(value = MemeConstants.EXT_URL_ALL)
    fun getAllDrinks(): Call<DrinksListObj>

    @GET(value = "search.php")
    suspend fun getDrinksByName(@Query("s") name: String): DrinksListObj
}