package com.practice.andromeme.network

import com.google.gson.GsonBuilder
import com.practice.andromeme.MemeConstants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetroFitBuilder {
    private val retrofit: Retrofit by lazy {
        val gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create()
        Retrofit.Builder()
            .baseUrl(MemeConstants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }
    val api: RetroFitServices by lazy {
        retrofit.create(RetroFitServices::class.java)
    }
}