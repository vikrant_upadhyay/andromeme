package com.practice.andromeme

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.practice.andromeme.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var bindingMain: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingMain = ActivityMainBinding.inflate(layoutInflater)
        setContentView(bindingMain.root)

        setupActionBarWithNavController(findNavController(R.id.graph_container))
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.graph_container)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}