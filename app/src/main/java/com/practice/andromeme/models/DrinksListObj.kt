package com.practice.andromeme.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DrinksListObj(
    @SerializedName("drinks")
    @Expose
    val drinks: List<Drink>

)