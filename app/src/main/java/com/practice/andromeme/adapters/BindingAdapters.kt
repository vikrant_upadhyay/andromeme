package com.practice.andromeme.adapters

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.practice.andromeme.models.Drink

@BindingAdapter("listdata")
fun bindtheRecycler(recyclerView: RecyclerView, data: List<Drink>?) {
    println("\n\t### CALLED ####\n\t VALUE:: "+data+"\n")
    val adapter = recyclerView.adapter as RecyclerViewAdapter
    adapter.submitList(data)
}

@BindingAdapter("useCoil")
fun usingGlidTosetImage(imageView: ImageView, data:String?){
//Glide.with(Fragment(R.layout.fragment_dashboard)).load(data).into(imageView)
imageView.load(data)
}