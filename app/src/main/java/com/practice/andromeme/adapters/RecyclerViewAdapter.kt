package com.practice.andromeme.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.practice.andromeme.R
import com.practice.andromeme.databinding.DrinksItemViewBinding
import com.practice.andromeme.models.Drink


class RecyclerViewAdapter : ListAdapter<Drink, RecyclerViewAdapter.DrinksViewHolder>(DiffCallBack) {

    class DrinksViewHolder(
        private var binding: DrinksItemViewBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(drink: Drink) {
            binding.drinkData = drink
            binding.executePendingBindings()
        }
    }

    companion object DiffCallBack : DiffUtil.ItemCallback<Drink>() {
        override fun areItemsTheSame(oldItem: Drink, newItem: Drink): Boolean {
            return oldItem.strDrink == newItem.strDrink || oldItem.strDrinkThumb == newItem.strDrinkThumb
        }

        override fun areContentsTheSame(oldItem: Drink, newItem: Drink): Boolean {
            return oldItem.strDrink == newItem.strDrink || oldItem.strDrinkThumb == newItem.strDrinkThumb
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinksViewHolder {
        return DrinksViewHolder(DrinksItemViewBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: DrinksViewHolder, position: Int) {
        val drinkItem = getItem(position)
        //In this we are just passing a single item of list
        holder.bind(drinkItem)
        holder.itemView.setOnClickListener {
            println("ITEM WAS CLICKED ${drinkItem.strDrink} ..!")
            val bundle = Bundle()
            bundle.putString("NAME", drinkItem.strDrink)
            val navController = Navigation.findNavController(holder.itemView)
            navController.navigate(R.id.action_dashboardFragment_to_drinkDetailFragment,bundle)

        }
    }
}