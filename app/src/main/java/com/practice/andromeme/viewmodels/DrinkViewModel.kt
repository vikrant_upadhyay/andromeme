package com.practice.andromeme.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.practice.andromeme.models.Drink
import com.practice.andromeme.models.DrinksListObj
import com.practice.andromeme.network.RetroFitBuilder
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DrinkViewModel : ViewModel() {

    private val _drinkList = MutableLiveData<List<Drink>>()
    var drinkList: MutableLiveData<List<Drink>> = _drinkList


    init {
        getServerData()
    }

    //GETTING ALL DATA FROM THE SERVER...
    fun getServerData() {
        viewModelScope.launch {
            val retrofitData = RetroFitBuilder.api.getAllDrinks()
            retrofitData.enqueue(object : Callback<DrinksListObj> {
                override fun onResponse(
                    call: Call<DrinksListObj>,
                    response: Response<DrinksListObj>
                ) {
                    println("\n\tRESPONSE:: " + response.body() + " \n\t")
                    _drinkList.value = response.body()?.drinks!!
                    println("\n\tRESPONSE:: " + _drinkList.value?.get(2)?.strDrinkThumb + " \n\t")
                }
                override fun onFailure(call: Call<DrinksListObj>, t: Throwable) {
                    println("\n\t ERROR:: " + t.localizedMessage.toString())
                }
            })

        }
    }

//GETTING DATA FROM SERVER FOR PARTICULAR NAME...
    fun getDataByNAme(name: String) {
        viewModelScope.launch {
            try {
                val objOne = RetroFitBuilder.api.getDrinksByName(name)
                val obj = objOne.drinks.get(0)
                println("\n\t#### obj:: "+obj.strDrink+"\n\t"+obj.strDrinkThumb
                        +"\n\t"+obj.strCategory+"\n\t"+obj.strGlass
                        +"\n\t"+obj.strIngredient11+"\n\t"+obj.strIngredient2
                        +"\n\t"+obj.strMeasure2+"\n\t"+obj.strInstructionsDE+"###\n\n"+obj)

            }catch (e:Exception){
                println("SOMETHING WENT WRONG!")
            }
        }
    }
}